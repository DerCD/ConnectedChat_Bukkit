package com.dercd.bukkit.plugins.connectedchat;

public class Messages
{
	public static final String
	playerNotFound = "playerNotFound",
	playerDisabledMsg = "playerDisabledMsg",
	playerDisabledMsgOrNotFound = "playerDisabledMsgOrNotFound",
	playerSilenced = "playerSilenced",
	serverDoesntResponse = "serverDoesntResponse",
	selfSilencedMsg = "selfSilencedMsg",
	selfDisabledMsg = "selfDisabledMsg",
	syntax = "syntax",
	selfIgnoringMsg = "selfIgnoringMsg",
	noReply = "noReply",
	selfMsg = "selfMsg";
	
}
