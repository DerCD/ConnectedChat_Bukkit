package com.dercd.bukkit.plugins.connectedchat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;
import com.dercd.bukkit.cdapi.listener.EventListener;
import com.dercd.bukkit.cdapi.tools.Tools.Minecraft;
import com.dercd.bukkit.plugins.connectedchat.CCPostVanishRequestEvent.QueryReason;

public class MessageProcessor implements Runnable
{
	List<JsonObject> queue = new ArrayList<JsonObject>();
	Lock queueLock = new ReentrantLock();
	ConnectedChat cc;
	EventListener el;
	
	public MessageProcessor(ConnectedChat cc)
	{
		this.cc = cc;
		this.el = cc.getHandler().getEventListener();
		new Thread(cc.getPluginThreadGroup(), this).start();
	}
	
	public void process(JsonObject jo) throws IOException
	{
		switch(jo.get("action").getAsString())
		{
			case "Vanish":
				returnVanish(jo);
				break;
			case "PlayerInfo":
				returnPlayerInfo(jo);
				break;
			case "Chat":
				procChat(jo);
				break;
			case "Silent":
				procSilent(jo);
				break;
			case "PlayerIgnorants":
				procIgnorants(jo);
				break;
			case "PlayerIgnore":
				procIgnore(jo);
				break;
			case "PlayerDIgnorants":
				procDIgnorants(jo);
				break;
			case "PlayerDIgnore":
				procDIgnore(jo);
				break;
			case "SVanish":
				returnVisibles(jo);
				break;
			case "Suggestions":
				returnSuggestions(jo);
				break;
		}
	}
	
	private void procSilent(JsonObject jo)
	{
		this.cc.silent.put(UUID.fromString(jo.get("uuid").getAsString()), jo.get("data").getAsBoolean());
	}
	
	private void procIgnorants(JsonObject jo)
	{
		Set<UUID> ignorants = new HashSet<UUID>();
		UUID uuid = UUID.fromString(jo.get("uuid").getAsString());
		JsonArray ja = jo.get("data").getAsJsonArray();
		for(JsonElement je : ja)
			ignorants.add(UUID.fromString(((JsonPrimitive) je).getAsString()));
		this.cc.ignores.put(uuid, ignorants);
	}
	private void procIgnore(JsonObject jo)
	{
		if(jo.get("data").getAsBoolean())
			this.cc.ignores.get(UUID.fromString(jo.get("uuid").getAsString())).add(UUID.fromString(jo.get("ignorant").getAsString()));
		else
			this.cc.ignores.get(UUID.fromString(jo.get("uuid").getAsString())).remove(UUID.fromString(jo.get("ignorant").getAsString()));
	}
	
	private void procDIgnorants(JsonObject jo)
	{
		Set<UUID> dignorants = new HashSet<UUID>();
		String name = jo.get("name").getAsString();
		JsonArray ja = jo.get("data").getAsJsonArray();
		for(JsonElement je : ja)
			dignorants.add(UUID.fromString(((JsonPrimitive) je).getAsString()));
		this.cc.dignores.put(name, dignorants);
	}
	private void procDIgnore(JsonObject jo)
	{
		if(jo.get("data").getAsBoolean())
			this.cc.dignores.get(jo.get("name").getAsString()).add(UUID.fromString(jo.get("ignorant").getAsString()));
		else
			this.cc.dignores.get(jo.get("name").getAsString()).remove(UUID.fromString(jo.get("ignorant").getAsString()));
	}
	
	private void returnSuggestions(JsonObject jo) throws IOException 
	{
		UUID requester = UUID.fromString(jo.get("requester").getAsString());
		String input = jo.get("input").getAsString().toLowerCase();
		JsonObject send = getUUIDJson("Suggestions", requester);
		JsonArray ja = new JsonArray();
		Boolean b;
		for(Player p : Minecraft.getOnlinePlayers())
			try
			{
				b = isPlayerVanished(p.getUniqueId(), requester, QueryReason.TABCOMPLETE);
				if(b == true && !this.cc.disabledBVM)
					b = !isPlayerVanished(requester, p.getUniqueId(), QueryReason.TABCOMPLETE);
				if(b == false && this.cc.getName(p).toLowerCase().startsWith(input))
					ja.add(new JsonPrimitive(p.getUniqueId().toString()));
			}
			catch(Exception x)
			{
				this.cc.clog.printException(x);
			}
		send.add("data", ja);
		this.cc.connection.send(send.toString());
	}
	private void returnVisibles(JsonObject jo) throws IOException
	{
		UUID requester = UUID.fromString(jo.get("requester").getAsString());
		JsonObject send = getUUIDJson("SVanish", requester);
		JsonArray ja = new JsonArray();
		Boolean b;
		for(Player p : Minecraft.getOnlinePlayers())
			try
			{
				b = isPlayerVanished(p.getUniqueId(), requester, QueryReason.SVANISH);
				if(b == false)
					ja.add(new JsonPrimitive(p.getUniqueId().toString()));
			}
			catch(Exception x)
			{
				this.cc.clog.printException(x);
			}
		send.add("data", ja);
		this.cc.connection.send(send.toString());
	}
	private void returnVanish(JsonObject jo) throws IOException
	{
		UUID uuid = UUID.fromString(jo.get("uuid").getAsString());
		UUID requesterUUID = UUID.fromString(jo.get("requester").getAsString());
		JsonObject send = getUUIDJson("Vanish", uuid);
		try
		{
			Boolean b = isPlayerVanished(uuid, requesterUUID, QueryReason.VANISH);
			if(b == null) return;
			send.addProperty("data", b);
		}
		catch (Exception x)
		{
			this.cc.clog.printException(x);
			send.addProperty("data", true);
		}
		this.cc.connection.send(send.toString());
	}
	public Boolean isPlayerVanished(UUID uuid, UUID requesterUUID, QueryReason qr)
	{
		boolean b = false;
		CCPreVanishRequestEvent prevre = new CCPreVanishRequestEvent(b, uuid, requesterUUID, qr);
		this.el.onEvent(prevre);
		Player p = Bukkit.getPlayer(prevre.getPlayer());
		if(p == null)
			throw new NullPointerException("PlayerUUID '" + prevre.getPlayer() + "' wasn't found");
		if(prevre.isCancelled())
		{
			this.cc.clog.log("VanishRequest cancelled by event", this);
			return null;
		}
		if(prevre.isStateForced()) return prevre.getState();
		b = prevre.getState();
		if (!b && this.cc.vmvnp != null)
		{
			this.cc.vmvnp.refreshManager();
			if(this.cc.vmvnp.vm != null && this.cc.vmvnp.vm.isVanished(p)) b = true;
		}
//		if(!b && this.cc.vmess != null && this.cc.vmess.ess.getUser(p).isVanished())
//			b = true;
		CCPostVanishRequestEvent postvre = new CCPostVanishRequestEvent(b, uuid, requesterUUID, qr);
		this.el.onEvent(postvre);
		if(postvre.isCancelled())
		{
			this.cc.clog.log("VanishRequest cancelled by event", this);
			return null;
		}
		return postvre.getState();
	}
	private void returnPlayerInfo(JsonObject jo) throws IOException
	{
		UUID uuid = UUID.fromString(jo.get("uuid").getAsString());
		PermissionUser pu = PermissionsEx.getPermissionManager().getUser(uuid);
		PlayerData pd = this.cc.disguised.get(uuid);
		JsonObject send = getUUIDJson("PlayerInfo", uuid);
		if(pd != null)
		{
			send.addProperty("displayName", pd.displayName);
			send.addProperty("prefix", pd.prefix.replace("&", "§"));
			send.addProperty("suffix", pd.suffix.replace("&", "§"));
		}
		else
		{
			send.addProperty("prefix", pu.getPrefix().replace("&", "§"));
			send.addProperty("suffix", pu.getSuffix().replace("&", "§"));
		}
		this.cc.connection.send(send.toString());
	}
	
	private void procChat(JsonObject jo)
	{
		this.cc.ccs.sendMessage(jo.get("data").getAsString());
	}	
	
	static JsonObject getBasicJson(String action)
	{
		JsonObject jo = new JsonObject();
		jo.addProperty("action", action);
		return jo;
	}
	static JsonObject getUUIDJson(String action, UUID uuid)
	{
		JsonObject jo = getBasicJson(action);
		jo.addProperty("uuid", uuid.toString());
		return jo;
	}
	
	
	@Override
	public void run()
	{
		try
		{
			JsonObject jo;
			while(true)
			{
				while(this.queue.size() != 0)
				{
					this.queueLock.lock();
					jo = this.queue.remove(0);
					this.queueLock.unlock();
					try
					{
						process(jo);
					}
					catch (Exception x)
					{
						printError(x, jo);
					}
				}
				secWait();
			}
		}
		catch(InterruptedException ox)
		{
			System.err.println("MessageProcessor-Thread: Interrupted");
		}
	}
	private void printError(Exception x, JsonObject jo)
	{
		this.cc.clog.log("--------------------", this);
		this.cc.clog.log("ConnectedChat:MessageProcessor: " + x.getClass().getName(), this);
		this.cc.clog.printException(x);
		this.cc.clog.log("Debug informations:", this);
		try
		{
			this.cc.clog.log("Json: " + jo, this);
		}
		catch (Throwable t)
		{
			this.cc.clog.log("Error while printing debug infos", this);
			this.cc.clog.printException(t);
		}
		this.cc.clog.log("End debug informations", this);
		this.cc.clog.log("--------------------", this);
	}
	
	private synchronized void secWait() throws InterruptedException
	{
		if(this.queue.size() != 0) return;
		wait();
	}
	public synchronized void secNotify()
	{
		notifyAll();
	}
}
