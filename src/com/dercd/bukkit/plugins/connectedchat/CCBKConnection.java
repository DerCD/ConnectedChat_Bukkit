package com.dercd.bukkit.plugins.connectedchat;

import java.io.IOException;
import com.google.gson.JsonObject;

import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.cdio.CDOutgoingConnection;
import com.dercd.cdio.CloseReason;

public class CCBKConnection extends CDOutgoingConnection
{
	Log clog;
	ConnectedChat cc;
	private static final String keyWord = "QVVuaWNvcm5IYXMxSG9ybg==";
	public static String host = "localhost";
	
	public CCBKConnection(ConnectedChat cc)
	{
		super(host, 23457);
		this.name = "ConnectedChat_BK";
		setKeyWordOut(keyWord.getBytes());
		this.threadGroup = cc.getPluginThreadGroup();
		this.cc = cc;
		this.clog = cc.clog;
		this.onConnected = (cdoc) ->
		{
			cdoc.protocol.afterVerification = (cdp) ->
			{
				this.clog.log("Sending server information", CCBKConnection.this);
				try
				{
					this.cc.sendInfo();
				}
				catch(Throwable t)
				{
					printException(t);
					this.close(CloseReason.OC_CON_BUILD_UP_FAILED);
				}
				this.clog.log("Done", this);
			};
			this.protocol.setDIO((s) -> { CCBKConnection.this.cc.addToQueue(s); });
		};
		this.onClosed = (cdoc, reason) ->
		{
			this.clog.log("Clearing received data", this);
			this.cc.mp.queueLock.lock();
			try
			{
				this.cc.mp.queue.clear();
			}
			finally
			{
				this.cc.mp.queueLock.unlock();
			}
			this.cc.silent.clear();
			this.cc.ignores.clear();
			this.cc.dignores.clear();
		};
	}
	
	public void send(JsonObject jo) throws IOException
	{
		send(jo.toString());
	}
}
