package com.dercd.bukkit.plugins.connectedchat;

import java.util.UUID;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CCPostVanishRequestEvent extends Event implements Cancellable
{
	private boolean state, cancelled, forced;
	private UUID player, requester;
	private final QueryReason reason;
	
	
	public static enum QueryReason
	{
		VANISH,
		SVANISH,
		TABCOMPLETE
	}
	
	public CCPostVanishRequestEvent(boolean state, UUID player, UUID requester, QueryReason qr)
	{
		this.state = state;
		this.player = player;
		this.requester = requester;
		this.reason = qr;
	}
	
	public void setState(boolean state)
	{
		this.state = state;
	}
	
	public boolean getState()
	{
		return this.state;
	}
	
	@Override
	public boolean isCancelled()
	{
		return this.cancelled;
	}
	@Override
	public void setCancelled(boolean cancelled)
	{
		this.cancelled = cancelled;
	}
	
	public QueryReason getReason()
	{
		return this.reason;
	}
	
	public boolean isStateForced()
	{
		return this.forced;
	}
	public void setStateForced(boolean force)
	{
		this.forced = force;
	}
	
	public UUID getPlayer()
	{
		return this.player;
	}
	public UUID getRequester()
	{
		return this.requester;
	}
	
	
	@Override
	public HandlerList getHandlers()
	{
		return null;
	}
}
