package com.dercd.bukkit.plugins.connectedchat;

import com.dercd.bukkit.cdapi.CDPlugin;

import org.kitteh.vanish.VanishManager;
import org.kitteh.vanish.VanishPlugin;

public class VanishManagerVNP
{
	VanishManager vm;
	CDPlugin cc;

	public VanishManagerVNP(CDPlugin cc)
	{
		this.cc = cc;
		this.vm = ((VanishPlugin) cc.getSoftDepend("VanishNoPacket")).getManager();
	}

	public void refreshManager()
	{
		if (this.vm == null)
			try
			{
				this.vm = ((VanishPlugin) this.cc.getSoftDepend("VanishNoPacket")).getManager();
			}
			catch (Exception x)
			{}
	}
}
