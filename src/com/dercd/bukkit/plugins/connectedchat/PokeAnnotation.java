package com.dercd.bukkit.plugins.connectedchat;

import java.util.UUID;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import com.dercd.bukkit.plugins.connectedchat.PokeAnnotation.DetachServerAnnotation.DetachType;

public abstract class PokeAnnotation
{
	public abstract JsonObject toJson();
	
	public String getName()
	{
		return getClass().getSimpleName();
	}
	
	public static class LetRequestPlayerAnnotation extends PokeAnnotation
	{
		UUID player;
		
		public LetRequestPlayerAnnotation(UUID player)
		{
			this.player = player;
		}
		
		@Override
		public JsonObject toJson()
		{
			return ConnectedChat.getUUIDJson("RequestPlayer", this.player);
		}
	}
	public static class DetachAnnotation extends PokeAnnotation
	{
		UUID player;
		
		public DetachAnnotation(UUID player)
		{
			this.player = player;
		}
		
		@Override
		public JsonObject toJson()
		{
			return ConnectedChat.getUUIDJson("GetPlayer", this.player);
		}
	}
	public static class AttachAnnotation extends PokeAnnotation
	{
		UUID player;
		
		public AttachAnnotation(UUID player)
		{
			this.player = player;
		}
		
		@Override
		public JsonObject toJson()
		{
			return ConnectedChat.getUUIDJson("TakePlayer", this.player);
		}
	}
	public static class DetachServerAnnotation extends PokeAnnotation
	{
		public static enum DetachType
		{
			CHAT,
			CMD,
			TAB,
			ALL
		}
		
		DetachType[] types;
		
		public DetachServerAnnotation(DetachType... types)
		{
			this.types = types;
		}
		
		@Override
		public JsonObject toJson()
		{
			JsonObject jo = ConnectedChat.getBasicJson("GetServer");
			JsonArray ja = new JsonArray();
			for(DetachType dt : this.types)
				ja.add(new JsonPrimitive(dt.name()));
			jo.add("types", ja);
			return jo;
		}
	}
	public static class AttachServerAnnotation extends PokeAnnotation
	{
		DetachType[] types;
		
		public AttachServerAnnotation(DetachType... types)
		{
			if(types.length == 0)
				this.types = new DetachType[] { DetachType.ALL };
			else
				this.types = types;
		}
		
		@Override
		public JsonObject toJson()
		{
			JsonObject jo = ConnectedChat.getBasicJson("GetServer");
			JsonArray ja = new JsonArray();
			for(DetachType dt : this.types)
				ja.add(new JsonPrimitive(dt.name()));
			jo.add("types", ja);
			return jo;
		}
	}
	public static class DisableBVMAnnotation extends PokeAnnotation
	{
		@Override
		public JsonObject toJson()
		{
			return ConnectedChat.getBasicJson("DisableBVM");
		}
	}
	public static class EnableBVMAnnotation extends PokeAnnotation
	{
		@Override
		public JsonObject toJson()
		{
			return ConnectedChat.getBasicJson("EnableBVM");
		}
	}
	public static class DisableDMBAnnotation extends PokeAnnotation
	{
		UUID player;
		
		public DisableDMBAnnotation(UUID player)
		{
			this.player = player;
		}
		
		@Override
		public JsonObject toJson()
		{
			return ConnectedChat.getUUIDJson("DisableDMB", this.player);
		}
	}
	public static class EnableDMBAnnotation extends PokeAnnotation
	{
		UUID player;
		
		public EnableDMBAnnotation(UUID player)
		{
			this.player = player;
		}
		
		@Override
		public JsonObject toJson()
		{
			return ConnectedChat.getUUIDJson("EnableDMB", this.player);
		}
	}
	public static class DisableServerDMBAnnotation extends PokeAnnotation
	{
		@Override
		public JsonObject toJson()
		{
			return ConnectedChat.getBasicJson("DisableServerDMB");
		}
	}
	public static class EnableServerDMBAnnotation extends PokeAnnotation
	{
		@Override
		public JsonObject toJson()
		{
			return ConnectedChat.getBasicJson("EnableServerDMB");
		}
	}
}
