package com.dercd.bukkit.plugins.connectedchat;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

import ru.tehkode.permissions.bukkit.PermissionsEx;
import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.annotations.CDPluginCommand;
import com.dercd.bukkit.cdapi.annotations.CDPluginDepend;
import com.dercd.bukkit.cdapi.annotations.CDPluginEvent;
import com.dercd.bukkit.cdapi.events.CDPluginEnableEvent;
import com.dercd.bukkit.cdapi.exceptions.CDInvalidArgsException;
import com.dercd.bukkit.cdapi.exceptions.CDNoPermissionException;
import com.dercd.bukkit.cdapi.tools.Tools.Data;
import com.dercd.cdio.CloseReason;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import net.minecraft.server.v1_10_R1.NBTTagCompound;
import net.minecraft.server.v1_10_R1.NBTTagList;
import net.minecraft.server.v1_10_R1.NBTTagString;

@CDPluginDepend(depends = { PermissionsEx.class }, softdepends = { "Essentials", "VanishNoPacket" })
public class ConnectedChat extends CDPlugin
{
	private String chatFormat;
	private List<String> networkIDs;
//	VanishManagerESS vmess = null;
	VanishManagerVNP vmvnp = null;
	Log clog;
	ConsoleCommandSender ccs = Bukkit.getConsoleSender();
	String getChat = null;
	private String mbeg = ChatColor.DARK_PURPLE + "[" + ChatColor.LIGHT_PURPLE + "ConnectedChat" + ChatColor.DARK_PURPLE + "] " + ChatColor.GOLD;
	String serverID;
	JsonParser jsonParser = new JsonParser();
	MessageProcessor mp;
	Lock silentAskingLock = new ReentrantLock();
	Lock ignorantsAskingLock = new ReentrantLock();
	Lock dignorantsAskingLock = new ReentrantLock();
	Map<UUID, PlayerData> disguised = new HashMap<UUID, PlayerData>();
	Map<UUID, Boolean> silent = new HashMap<UUID, Boolean>();
	Map<UUID, Set<UUID>> ignores = new HashMap<UUID, Set<UUID>>();
	Map<String, Set<UUID>> dignores = new HashMap<String, Set<UUID>>();
	boolean disabledBVM = false;
	boolean disabledServerDMB = false;
	private List<PokeAnnotation> pokeAnnotations = new ArrayList<PokeAnnotation>();
	public CCBKConnection connection;
	
	public ConnectedChat(PluginHandler handler)
	{
		super(handler);
		this.clog = handler.getCLog();
	}

	@Override
	public String getVersion()
	{
		return "0.5.1_1501231726";
	}

	@Override
	public final String getAuthor()
	{
		return "CD";
	}

	@CDPluginEvent(priority = 9500)
	public void onEnable(CDPluginEnableEvent e) throws IOException
	{
		this.mp = new MessageProcessor(this);
		this.clog.log("Getting soft-depends", this);
		getVPlugins();
		this.clog.log("Done", this);
		this.clog.log("Loading Data", this);
		boolean active = load();
		if(active) this.clog.log("Success", this);
		else
		{
			this.clog.log("No success", this);
			this.clog.log("ConnectedChat won't be available until a successfull load", this);
			e.setSuccess(this, false);
			return;
		}
		this.clog.log("Starting Connection-Thread", this);
		this.connection = new CCBKConnection(this);
		this.connection.out = this.clog.getConsumer("[ConnectedChat] ");
		this.connection.threadGroup = this.getPluginThreadGroup();
		this.connection.run();
		this.clog.log("Done", this);
	}

	@Override
	public Permission[] getPermissions()
	{
		return new Permission[]
		{
			new Permission("cd.cc", PermissionDefault.OP),
			new Permission("cd.cc.io", PermissionDefault.OP)
		};
	}

	@Override
	public String[] getDirectorys()
	{
		return new String[] { "ConnectedChat" };
	}

	@CDPluginCommand(commands = { "ignore", "silent", "msg", "t", "tell", "w", "whisper", "r", "reply", "msgtoggle", "telltoggle", "wtoggle", "bukkit:msg", "bukkit:w", "bukkit:whisper", "bukkit:tell", "ac", "bukkit:ac", "mute", "unmute" }, priority = 1000)
	public void onCCCommand(CommandEvent e) throws IOException
	{
		Player p = (Player) e.getSender();
		this.clog.debug("Sending PlayerCommand to Proxy: " + e.getFullCommand(), this);
		JsonObject jo = getUUIDJson("PlayerCommand", p.getUniqueId());
		jo.addProperty("data", "/" + (e.getFullCommand().startsWith("bukkit:") ? e.getFullCommand().substring(7) : e.getFullCommand()));
		this.connection.send(jo.toString());
	}

	@CDPluginCommand(commands = { "conchat cd.cc 1" })
	public void onCommand(CommandEvent e) throws CDInvalidArgsException, CDNoPermissionException, IOException
	{
		String[] args = e.getArgs();
		e.validateCount(1, -1);
		switch (args[0].toLowerCase())
		{
			case "load":
				load(e.getSender());
				return;
			case "save":
				save(e.getSender());
				return;
			case "getchat":
				getChat((Player) e.getSender());
				return;
			case "detach":
				e.validateCount(2);
				detachPlayer(UUID.fromString(args[1]));
				return;
			case "attach":
				e.validateCount(2);
				attachPlayer(UUID.fromString(args[1]));
				return;
			case "close":
			case "kill":
				e.validateCount(1);
				this.connection.close(CloseReason.MANUAL);
				e.getSender().sendMessage(this.mbeg + "Socket closed");
				e.getSender().sendMessage(this.mbeg + "Opening new connection in a couple of seconds");
				return;
		}
		throw new CDInvalidArgsException(e.getCommand().getName());
	}

	private void getVPlugins()
	{
//		if (hasSoftDepend("Essentials"))
//			this.vmess = new VanishManagerESS(this);
		if (hasSoftDepend("VanishNoPacket"))
			this.vmvnp = new VanishManagerVNP(this);
	}

	public boolean getSilentSync(UUID player)
	{
		this.silentAskingLock.lock();
		try
		{
			Boolean back;
			if ((back = this.silent.get(player)) == null)
			{
				JsonObject jo = getUUIDJson("GetSilent", player);
				this.connection.send(jo);
				for (int i = 0; i < 50 && this.silent.get(player) == null; i++)
					Thread.sleep(100);
				if((back = this.silent.get(player)) == null) return false;
				return back;
			}
			return back;
		}
		catch (Exception x)
		{
			this.clog.log("Error while asking silent-status for " + player.toString(), this);
			return false;
		}
		finally
		{
			this.silentAskingLock.unlock();
		}
	}
	public Set<UUID> getIgnorantsSync(UUID player)
	{
		this.ignorantsAskingLock.lock();
		try
		{
			Set<UUID> back;
			if ((back = this.ignores.get(player)) == null)
			{
				JsonObject jo = getUUIDJson("GetIgnorants", player);
				this.connection.send(jo);
				for (int i = 0; i < 50 && this.ignores.get(player) == null; i++)
					Thread.sleep(100);
				if((back = this.ignores.get(player)) == null) return new HashSet<UUID>();
				return back;
			}
			return back;
		}
		catch(Exception x)
		{
			this.clog.log("Error while asking ignorants of " + player.toString(), this);
			return new HashSet<UUID>();
		}
		finally
		{
			this.ignorantsAskingLock.unlock();
		}
	}
	public Set<UUID> getDgnorantsSync(String name)
	{
		this.dignorantsAskingLock.lock();
		try
		{
			Set<UUID> back;
			if ((back = this.dignores.get(name)) == null)
			{
				JsonObject jo = getBasicJson("GetDIgnorants");
				jo.addProperty("name", name);
				this.connection.send(jo);
				for (int i = 0; i < 50 && this.dignores.get(name) == null; i++)
					Thread.sleep(100);
				if((back = this.dignores.get(name)) == null) return new HashSet<UUID>();
				return back;
			}
			return back;
		}
		catch(Exception x)
		{
			this.clog.log("Error while asking dignorants of " + name, this);
			return new HashSet<UUID>();
		}
		finally
		{
			this.dignorantsAskingLock.unlock();
		}
	}
	
	public void detachPlayer(UUID player) throws IOException
	{
		this.clog.log("Sending detach-request for " + player.toString(), this);
		this.connection.send(new PokeAnnotation.DetachAnnotation(player).toJson().toString());
	}
	public void attachPlayer(UUID player) throws IOException
	{
		this.clog.log("Sending attach-request for " + player.toString(), this);
		this.connection.send(new PokeAnnotation.AttachAnnotation(player).toJson().toString());
	}
	
	public void detachServer() throws IOException
	{
		this.clog.log("Sending Server-detach-request", this);
		this.connection.send(new PokeAnnotation.DetachServerAnnotation().toJson().toString());
	}
	public void attachServer() throws IOException
	{
		this.clog.log("Sending Server-attach-request", this);
		this.connection.send(new PokeAnnotation.AttachServerAnnotation().toJson().toString());
	}
	
	public void disableBVM() throws IOException
	{
		if(!this.isEnabled())
		{
			this.clog.log("Adding DisableBVM-request to serverinfo", this);
			this.addPokeAnnotation(new PokeAnnotation.DisableBVMAnnotation());
		}
		else
		{
			this.clog.log("Sending DisableBVM-request", this);
			this.connection.send(new PokeAnnotation.DisableBVMAnnotation().toJson().toString());
		}
		this.disabledBVM = true;
	}
	public void enableBVM() throws IOException
	{
		if(!this.isEnabled())
			this.clog.log("Don't send EnableBVM-request because serverinfo wasn't sent yet", this);
		else
		{
			this.clog.log("Sending EnableBVM-request", this);
			this.connection.send(new PokeAnnotation.EnableBVMAnnotation().toJson().toString());
		}
		this.disabledBVM = false;
	}
	
	public void disableServerDMB() throws IOException
	{
		if(!this.isEnabled())
		{
			this.clog.log("Adding DisableServerDMB-request to serverinfo", this);
			this.addPokeAnnotation(new PokeAnnotation.DisableServerDMBAnnotation());
		}
		else
		{
			this.clog.log("Sending DisableServerDMB-request", this);
			this.connection.send(new PokeAnnotation.DisableServerDMBAnnotation().toJson().toString());
		}
		this.disabledServerDMB = true;
	}
	public void enableServerDMB() throws IOException
	{
		if(!this.isEnabled())
			this.clog.log("Don't send EnableServerDMB-request because serverinfo wasn't sent yet", this);
		else
		{
			this.clog.log("Sending EnableServerDMB-request", this);
			this.connection.send(new PokeAnnotation.EnableServerDMBAnnotation().toJson().toString());
		}
		this.disabledServerDMB = false;
	}
	public void disableDMB(UUID player) throws IOException
	{
		this.clog.log("Sending DisableDMB-request for " + player.toString(), this);
		this.connection.send(new PokeAnnotation.DisableDMBAnnotation(player).toJson().toString());
	}
	public void enableDMB(UUID player) throws IOException
	{
		this.clog.log("Sending EnableDMB-request for " + player.toString(), this);
		this.connection.send(new PokeAnnotation.EnableDMBAnnotation(player).toJson().toString());
	}
	
	public void sendCCMessage(String messageId, UUID player) throws IOException
	{
		JsonObject jo = getUUIDJson("Message", player);
		jo.addProperty("data", messageId);
		this.connection.send(jo.toString());
	}
	
	public void letRequestPlayer(UUID player) throws IOException
	{
		this.connection.send(new PokeAnnotation.LetRequestPlayerAnnotation(player).toJson().toString());
	}
	
	public void undisguisePlayer(UUID player) throws IOException
	{
		this.clog.log("Undisguising " + player, this);
		this.disguised.remove(player);
		revokePlayer(player);
	}
	public void disguisePlayer(UUID player, String prefix, String suffix, String name) throws IOException
	{
		this.clog.log("Disguising " + player + " as " + prefix + "-" + name + "-" + suffix, this);
		this.disguised.put(player, new PlayerData(prefix, suffix, name, player));
		revokePlayer(player);
		letRequestPlayer(player);
	}
	
	public void revokePlayer(UUID player) throws IOException
	{
		this.connection.send(getUUIDJson("RevokePlayer", player).toString());
	}

	@CDPluginEvent(priority = 0, lateRegister = true)
	public void getChat(AsyncPlayerChatEvent e)
	{
		if (this.getChat == null)
			return;
		Player p = e.getPlayer();
		if (!p.getName().equals(this.getChat))
			return;
		this.getChat = null;
		e.setCancelled(true);
		this.chatFormat = e.getFormat();
		try
		{
			save();
			p.sendMessage(this.mbeg + "Chat format successfully get");
			this.connection.close(CloseReason.MANUAL);
			p.sendMessage(this.mbeg + "Socket closed");
			p.sendMessage(this.mbeg + "Open new connection");
		}
		catch (Exception x)
		{
			this.clog.printException(x);
			e.getPlayer().sendMessage(this.mbeg + ChatColor.RED + "The data was received, but there's a problem in saving the Data");
		}
	}
	private void getChat(Player p)
	{
		this.getChat = p.getName();
		this.clog.log("Sending Test-Message from " + this.getChat + " to get ChatFormat", this);
		p.chat("te\nst");
	}
	
	protected void sendInfo() throws IOException
	{
		if (this.chatFormat == null)
		{
			this.clog.warn("No chat format configured", this);
			return;
		}
		if(this.networkIDs.size() == 0)
		{
			this.clog.warn("Zero networks configured", this);
			return;
		}
		JsonObject jo = getBasicJson("Info");
		jo.addProperty("chatFormat", this.chatFormat);
		JsonArray networkIDs = new JsonArray();
		for(String networkID : this.networkIDs)
			networkIDs.add(new JsonPrimitive(networkID));
		jo.add("networkIDs", networkIDs);
		JsonArray additional = new JsonArray();
		for(PokeAnnotation pa : this.pokeAnnotations)
			additional.add(pa.toJson());
		for(UUID u : this.disguised.keySet())
			additional.add(new PokeAnnotation.LetRequestPlayerAnnotation(u).toJson());
		jo.add("additional", additional);
		jo.addProperty("id", this.serverID);
		this.connection.send(jo.toString());
	}

	private void save(CommandSender s) throws CDNoPermissionException
	{
		if (!s.hasPermission("cd.cc.io")) throw new CDNoPermissionException(true);
		try
		{
			save();
			s.sendMessage(ChatColor.GREEN + "Data saved");
		}
		catch (Exception x)
		{
			this.clog.printException(x);
			s.sendMessage(ChatColor.RED + "Error while saving Data");
		}
	}
	private void load(CommandSender s) throws CDNoPermissionException
	{
		if (!s.hasPermission("cd.cc.io")) throw new CDNoPermissionException(true);
		try
		{
			load();
			this.clog.log("Starting Connection-Thread", this);
			if(this.connection == null)
			{
				this.connection = new CCBKConnection(this);
				this.connection.run();
			}
			else
			{
				s.sendMessage(this.mbeg + "Closing socket");
				this.connection.close(CloseReason.MANUAL);
			}
			s.sendMessage(this.mbeg + "Open new connection");
			this.clog.log("Done", this);
		}
		catch (Exception x)
		{
			this.clog.printException(x);
			s.sendMessage(ChatColor.RED + "Error while loading Data");
		}
	}
	private void save() throws IOException
	{
		this.clog.log("Saving Data", this);
		NBTTagCompound base = new NBTTagCompound();
		base.set("chatFormat", new NBTTagString(this.chatFormat));
		NBTTagList networkIDs = new NBTTagList();
		for(String networkID : this.networkIDs)
			networkIDs.add(new NBTTagString(networkID));
		base.set("networkIDs", networkIDs);
		base.setString("serverID", this.serverID);
		this.clog.log("Writing data", this);
		Data.save(base, getDir() + getDirectorys()[0] + "/data.dat", this);
	}
	private boolean load() throws IOException
	{
		try
		{
			NBTTagCompound base = Data.load(getDir() + getDirectorys()[0] + "/data.dat", this);
			if (base == null) return false;
			this.chatFormat = base.getString("chatFormat");
			this.networkIDs = new ArrayList<String>();
			NBTTagList networkIDs = (NBTTagList) base.get("networkIDs");
			for (int i = 0; i < networkIDs.size(); i++)
				this.networkIDs.add(networkIDs.getString(i));
			this.serverID = base.getString("serverID");
			if(base.hasKey("host"))
				CCBKConnection.host = base.getString("host");
			this.clog.log("Data loaded", this);
			return true;
		}
		catch (Exception x)
		{
			if(x instanceof IOException) throw x;
			return false;
		}
	}

	public void addPokeAnnotation(PokeAnnotation pa)
	{
		this.pokeAnnotations.add(pa);
		this.clog.log("PokeAnnotation '" + pa.getClass().getSimpleName() + "' with Json " + pa.toJson().toString() + " was added", this);
		switch(pa.getName())
		{
			case "DisableBVMAnnotation":
				this.clog.log("Disabled BVM", this);
				this.disabledBVM = true;
				break;
			case "EnableBVMAnnotation":
				this.clog.log("Enabled BVM", this);
				this.disabledBVM = false;
				break;
			case "DisableServerDMBAnnotation":
				this.clog.log("Disabled ServerDMB", this);
				this.disabledServerDMB = true;
				break;
			case "EnableServerDMBAnnotation":
				this.clog.log("Enabled ServerDMB", this);
				this.disabledServerDMB = false;
				break;
		}
	}	
	
	protected void addToQueue(String json)
	{
		JsonObject jo;
		try { jo = (JsonObject) this.jsonParser.parse(json); }
		catch (Exception x)
		{
			this.clog.log("Error while parsing json '" + json + "'", this);
			return;
		}
		this.mp.queueLock.lock();
		try { this.mp.queue.add(jo); this.mp.secNotify(); }
		finally { this.mp.queueLock.unlock(); }
	}
	
	public boolean isBVMDisabled()
	{
		return this.disabledBVM;
	}
	public boolean isServerDMBDisabled()
	{
		return this.disabledServerDMB;
	}
	
	public String getName(Player p)
	{
		return this.disguised.containsKey(p.getUniqueId()) ? this.disguised.get(p.getUniqueId()).displayName : p.getName();
	}
	
	static JsonObject getBasicJson(String action)
	{
		return MessageProcessor.getBasicJson(action);
	}
	static JsonObject getUUIDJson(String action, UUID uuid)
	{
		return MessageProcessor.getUUIDJson(action, uuid);
	}
}

class PlayerData
{
	public String prefix, suffix, displayName;
	private UUID u;

	public PlayerData(UUID u)
	{
		this("", "", "", u);
	}

	public PlayerData(String prefix, String suffix, String displayName, UUID u)
	{
		this.prefix = prefix;
		this.suffix = suffix;
		this.displayName = displayName;
		this.u = u;
	}

	public UUID getUUID()
	{
		return this.u;
	}
}
